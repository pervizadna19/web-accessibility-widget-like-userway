/**
 * @author Parviz Mammadov
 * @name WebAccessibilityWidget
 * 
 */
var fontSizeIncrease = 1.2;
var lineHeightIncrease = 1.2;
var textSpaceIncrease = 1.2;
var increaseCount = {
    fontSize: 3,
    lineHeight: 3,
    textSpace: 3
}

var clickindex = {
    fontSize: 0,
    lineHeight: 0,
    textSpace: 0
}
var isReadingGuide = false;
var isReadingMask = false;


$(document).ready(function() {
    $('.mp-a-operation-click').click(function() {
        let type = $(this).attr("data-type");
        let elements = $('body').find('*');
        if (type == 'fs-plus') {
            let isChange = clickIndexChange("fontSize", true);
            if (isChange) {
                $.each(elements, function(index, element) {
                    fontSizeChange(index, element, true)
                });
            }

        } // font size plus end
        else if (type == 'fs-minus') {
            let isChange = clickIndexChange("fontSize");
            if (isChange) {
                $.each(elements, function(index, element) {
                    fontSizeChange(index, element)
                });
            }

        } // font size minus end
        else if (type == 'fs-redo') {
            clickindex.fontSize = 0;
            $.each(elements, function(index, element) {
                // element.style.removeProperty('font-family');
                $(element).css("font-size", "")
            });
        } // font size redo end
        else if (type == 'lh-plus') {
            let isChange = clickIndexChange("lineHeight", true);
            if (isChange) {
                $.each(elements, function(index, element) {
                    // element.style.removeProperty('font-family');
                    // $(element).css("font-size", "")
                    lineHeightChange(index, element, true);
                });
            }
        } // font size redo end
        else if (type == 'lh-minus') {
            let isChange = clickIndexChange("lineHeight");
            if (isChange) {
                $.each(elements, function(index, element) {
                    // element.style.removeProperty('font-family');
                    // $(element).css("font-size", "")
                    lineHeightChange(index, element);
                });
            }
        } // font size redo end
        else if (type == 'lh-redo') {
            clickindex.lineHeight = 0;
            $.each(elements, function(index, element) {
                // element.style.removeProperty('font-family');
                $(element).css("line-height", "")
            });
        } // font size redo end
        else if (type == 'lh-redo') {
            clickindex.lineHeight = 0;
            $.each(elements, function(index, element) {
                // element.style.removeProperty('font-family');
                $(element).css("line-height", "")
            });
        } // font size redo end
        else if (type == 'rg-start') {
            isReadingGuide = true;
            readingGuide();
        } // font size redo end
        else if (type == 'rg-redo') {
            isReadingGuide = false;
            readingGuide();
        } // font size redo end
        else if (type == 'ht-set') {
            highLightLink(true);
        } else if (type == 'ht-redo') {
            highLightLink();
        } else if (type == 'ts-plus') {
            let isChange = clickIndexChange("textSpace", true);
            if (isChange) {
                $.each(elements, function(index, element) {
                    textSpaceChange(index, element, true)
                });
            }

        } // font size plus end
        else if (type == 'ts-minus') {
            let isChange = clickIndexChange("textSpace");
            if (isChange) {
                $.each(elements, function(index, element) {
                    textSpaceChange(index, element)
                });
            }

        } // font size minus end
        else if (type == 'ts-redo') {
            clickindex.textSpace = 0;
            $.each(elements, function(index, element) {
                // element.style.removeProperty('font-family');
                $(element).css("letter-spacing", "")
            });
        } // font size redo end
        else if (type == 'rm-start') {
            isReadingMask = true;
            readingMask();
        } // font size redo end
        else if (type == 'rm-redo') {
            isReadingMask = false;
            readingMask();
        } // font size redo end
        else if (type == 'color-invert') {
            invertColor(true);
        } else if (type == 'color-invert-redo') {
            invertColor();
        } else if (type == 'color-hue') {
            hueColor(true);
        } else if (type == 'color-hue-redo') {
            hueColor();
        } else if (type == 'color-dark') {
            darkColor(true);
        } else if (type == 'color-dark-redo') {
            darkColor();
        }

    })
});

function clickIndexChange(type = "fontSize", isPlus = false) {
    let ip = clickindex[type];
    let im = clickindex[type];
    ip++;
    im--;

    if (isPlus) {
        if (increaseCount[type] >= ip) { clickindex[type]++; return true; }
        return false;
    } else {
        if (im >= -1 * increaseCount[type]) {
            clickindex[type]--;
            return true;
        }
        return false;
    }
}

function fontSizeChange(index, element, type = false) {
    if (keepWidgetStyle(element)) return false;
    let e = $(element);
    let fontSizeOld = parseFloat(e.css("font-size"));

    if (type) {
        fontSizeOld = fontSizeOld * fontSizeIncrease;

    } else {
        fontSizeOld = fontSizeOld / fontSizeIncrease;
    }
    e.css("font-size", fontSizeOld + 'px')
}

function lineHeightChange(index, element, type = false) {
    if (keepWidgetStyle(element)) return false;
    let e = $(element);
    let lineHeightOld;
    if (clickindex.lineHeight == 1 || clickindex.lineHeight == -1) {
        lineHeightOld = parseFloat(e.css("font-size"));
    } else {
        lineHeightOld = parseFloat(e.css("line-height"));
    }

    if (type) {
        lineHeightOld = lineHeightOld * lineHeightIncrease;
        e.css("line-height", lineHeightOld + 'px')

    } else {
        lineHeightOld = lineHeightOld / lineHeightIncrease;
        e.css("line-height", lineHeightOld + 'px')

    }

}


function readingGuide() {
    if (isReadingGuide) {
        $('.mp-a-reading-guide').show();
        $("body").mousemove(function(event) {
            // var pageCoords = "( " + event.pageX + ", " + event.pageY + " )";
            // var clientCoords = "( " + event.clientX + ", " + event.clientY + " )";
            $('.mp-a-reading-guide').css("left", event.pageX - 300 + "px");
            $('.mp-a-reading-guide').css("top", event.pageY + 50 + "px");
        });
    } else {
        $('.mp-a-reading-guide').hide();
    }
}

function highLightLink(type = false) {
    if (type) {
        $("a").each(function() {
            var _t = $(this);
            _t.css('background', '#000');
            _t.css('color', 'yellow')
        })
    } else {
        $("a").each(function() {
            var _t = $(this);
            _t.css('background', '');
            _t.css('color', '')
        })
    }
}

function textSpaceChange(index, element, type = false) {
    if (keepWidgetStyle(element)) return false;
    let e = $(element);
    let textSpaceOld = parseFloat(e.css("letter-spacing"));
    textSpaceOld = textSpaceOld < 1 ? 1 : textSpaceOld;
    if (type) {
        textSpaceOld = textSpaceOld * textSpaceIncrease;

    } else {
        textSpaceOld = textSpaceOld / textSpaceIncrease;
    }
    e.css("letter-spacing", textSpaceOld + 'px')
}

function readingMask() {
    if (isReadingMask) {
        $('.mp-a-rm-top').show();
        $('.mp-a-rm-bottom').show();
        $("body").mousemove(function(event) {
            let w = window.screen.height;

            // var pageCoords = "( " + event.pageX + ", " + event.pageY + " )";
            // var clientCoords = "( " + event.clientX + ", " + event.clientY + " )";
            $('.mp-a-rm-top').css("height", event.pageY - 50 + "px");
            $('.mp-a-rm-bottom').css("height", w - event.pageY - 200 + "px");
        });
    } else {
        $('.mp-a-rm-top').hide();
        $('.mp-a-rm-bottom').hide();
    }
}

function invertColor(type = false) {
    if (type) {
        $('body').addClass("mp-a-invertColor")
    } else {
        $('body').removeClass("mp-a-invertColor")
    }
}

function hueColor(type = false) {
    if (type) {
        $('body').addClass("mp-a-hueColor")
    } else {
        $('body').removeClass("mp-a-hueColor")
    }
}

function darkColor(type = false) {
    if (type) {
        $('body').addClass("mp-a-darmode")
    } else {
        $('body').removeClass("mp-a-darmode")
    }
}

function keepWidgetStyle(element) {
    var $el = $(element);
    var className = $el.attr("class");
    if (className) {
        if (className.includes("mp-a")) {
            return true;
        }
    }
    return false;
}